//1. Tìm số nguyên dương nhỏ nhất sao cho:
// 1 + 2 + … + n > 10000
var n = 0;
var sum = 0;
while(sum < 10000)
{
    n++;
    sum += n;
}
document.getElementById("bai1").innerHTML = n;
// Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2
// + x^3 + … + x^n (Sử dụng vòng lặp và hàm)
function tinhbai2(){
    var nhapx = document.getElementById("nhap-x").value;
    var nhapn = document.getElementById("nhap-n").value;
    var tong = 0;
    var tich = 1;
    for (var i = 1; i <= nhapn; i++)
    {
        tich = tich * nhapx;
        tong += tich;
    }
    document.getElementById("bai2").innerHTML = tong;

}
// Nhập vào n. Tính giai thừa 1*2*...n
function tinhGiaiThua(){
    var inputN = document.getElementById("nhap-n-bai3").value;
    var giaithua = 1;
    for(var i = 1; i <= inputN; i++)
    {
        giaithua *= i;
    }
    document.getElementById("bai3").innerHTML = giaithua;
}
// Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div.
// Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì
// background màu xanh.
function taoDiv(){
    var nhapsoluong = document.getElementById("nhap-so-luong").value;
    for (var i = 0; i <nhapsoluong; i++)
    {
        if (i%2 == 0)
        {   
        var boxEle = document.createElement('div');
        var container = document.querySelector('.app');
        boxEle.style.width = '100px';
        boxEle.style.height = '30px';
        boxEle.style.backgroundColor = 'red';
        container.appendChild(boxEle);
    }
    else{
        var boxEle = document.createElement('div');
        var container = document.querySelector('.app');
        boxEle.style.width = '100px';
        boxEle.style.height = '30px';
        boxEle.style.backgroundColor = 'blue';
        container.appendChild(boxEle);
    }
}
}